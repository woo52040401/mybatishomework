-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 伺服器版本:                        8.0.18 - MySQL Community Server - GPL
-- 伺服器作業系統:                      Win64
-- HeidiSQL 版本:                  10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 傾印  資料表 jdbc.employee 結構
CREATE TABLE IF NOT EXISTS `employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  `ENNAME` varchar(50) NOT NULL,
  `TALL` int(11) NOT NULL,
  `WEIGHT` int(11) NOT NULL,
  `PHONE` varchar(50) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `BMI` float NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在傾印表格  jdbc.employee 的資料：~56 rows (近似值)
DELETE FROM `employee`;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`ID`, `NAME`, `ENNAME`, `TALL`, `WEIGHT`, `PHONE`, `EMAIL`, `BMI`, `createdAt`, `updatedAt`) VALUES
	(1, '廖強原', 'Edward-Liaw', 181, 64, '0215', 'WGWEG@transglobe.com.tw', 19.5354, '2020-03-20 15:03:05', '2020-03-20 15:04:02'),
	(2, '企鵝', 'Tom', 177, 80, '2115', 'AaronHuang@transglobe.com.tw', 25.5354, '2020-03-20 15:03:05', '2020-03-20 15:04:02'),
	(3, '黃大羚', 'Aya-Wong', 154, 69, '6144', 'ayawong@transglobe.com.tw', 29.0943, '2020-03-20 15:03:05', NULL),
	(4, '陳美鴻', 'Alvin-Chen', 163, 53, '6121', 'AlvinChen@transglobe.com.tw', 19.9481, '2020-03-20 15:03:05', NULL),
	(5, '洪毅芳', 'Amy-Hung', 182, 88, '6190', 'AmyHung@transglobe.com.tw', 26.5668, '2020-03-20 15:03:05', NULL),
	(6, '王雯妮', 'Annie-Wang', 160, 88, '6159', 'AnnieWang@transglobe.com.tw', 34.375, '2020-03-20 15:03:05', NULL),
	(7, '薛安承', 'Cosmo-Hsueh', 189, 69, '6140', 'CosmoHsueh@transglobe.com.tw', 19.3164, '2020-03-20 15:03:05', NULL),
	(8, '陳祐森', 'Disen-Chen', 175, 66, '6139', 'disenchen@transglobe.com.tw', 21.551, '2020-03-20 15:03:05', NULL),
	(9, '張迪道', 'Douglas-Chang', 151, 58, '6119', 'DouglasChang@transglobe.com.tw', 25.4375, '2020-03-20 15:03:05', NULL),
	(10, '高守緯', 'George-Gao', 165, 56, '6118', 'GeorgeGao@transglobe.com.tw', 20.5693, '2020-03-20 15:03:05', NULL),
	(11, '張至豪', 'Howard-Chang', 154, 71, '6147', 'HowardChang@transglobe.com.tw', 29.9376, '2020-03-20 15:03:05', NULL),
	(12, '劉哲航', 'James-Liu', 174, 75, '6143', 'JamesLiu@transglobe.com.tw', 24.7721, '2020-03-20 15:03:05', NULL),
	(13, '王嘉銘', 'Jeffrey-Wang', 186, 71, '6127', 'JeffreyWang@transglobe.com.tw', 20.5226, '2020-03-20 15:03:05', NULL),
	(14, '鄭潘毅', 'Jeremy-Cheng', 181, 55, '6174', 'jeremycheng@transglobe.com.tw', 16.7883, '2020-03-20 15:03:06', NULL),
	(15, '楊唯霖', 'Jet-Yang', 153, 75, '2113', 'jetyang@transglobe.com.tw', 32.039, '2020-03-20 15:03:06', NULL),
	(16, '王杰凱', 'Jobim-Wang', 185, 58, '6180', 'jobimwang@transglobe.com.tw', 16.9467, '2020-03-20 15:03:06', NULL),
	(17, '沈揚妤', 'Kate-Shen', 185, 85, '6137', 'KateShen@transglobe.com.tw', 24.8356, '2020-03-20 15:03:06', NULL),
	(18, '施婕秀', 'Linda-Shih', 187, 52, '2111', 'lindashih@transglobe.com.tw', 14.8703, '2020-03-20 15:03:06', NULL),
	(19, '陳孟汝', 'Maggie-Chen', 170, 85, '6127', 'maggiechen@transglobe.com.tw', 29.4118, '2020-03-20 15:03:06', NULL),
	(20, '余玫澤', 'Martin-Yu', 161, 73, '6148', 'martinyu@transglobe.com.tw', 28.1625, '2020-03-20 15:03:06', NULL),
	(21, '楊承銘', 'Obastan-Yang', 187, 52, '6133', 'obastanyang@transglobe.com.tw', 14.8703, '2020-03-20 15:03:06', NULL),
	(22, '熊文芸', 'Phyllis-Hsiung', 189, 72, '6122', 'PhyllisHsiung@transglobe.com.tw', 20.1562, '2020-03-20 15:03:06', NULL),
	(23, '張若宇', 'Shaoyu-Chang', 178, 50, '6157', 'ShaoyuChang@transglobe.com.tw', 15.7808, '2020-03-20 15:03:06', NULL),
	(24, '符少玲', 'Shirley-Fu', 183, 54, '6129', 'shirleyfu@transglobe.com.tw', 16.1247, '2020-03-20 15:03:06', NULL),
	(25, '劉聖緯', 'Steve-Liu', 187, 72, '6123', 'SteveLiu@transglobe.com.tw', 20.5897, '2020-03-20 15:03:06', NULL),
	(26, '謝俊燕', 'Yetta-Shieh', 176, 84, '6126', 'yettashieh@transglobe.com.tw', 27.1178, '2020-03-20 15:03:06', NULL),
	(27, '林青承', 'YuCheng-Lin', 169, 87, '6128', 'YuChengLin@transglobe.com.tw', 30.4611, '2020-03-20 15:03:06', NULL),
	(28, '張昱琳', 'Christine-Chang', 183, 54, '6157', 'christinechang@transglobe.com.tw', 16.1247, '2020-03-20 15:03:06', NULL),
	(29, '蔡琬炯', 'Akino-Tsai', 187, 72, '6106', 'akinotsai@transglobe.com.tw', 20.5897, '2020-03-20 15:03:06', NULL),
	(30, '嚴銘莉', 'Amery-Yen', 176, 84, '6141', 'ameryyen@transglobe.com.tw', 27.1178, '2020-03-20 15:03:06', NULL),
	(31, '張清興', 'Arthur-Chang', 169, 87, '6164', 'arthurchang@transglobe.com.tw', 30.4611, '2020-03-20 15:03:06', NULL),
	(32, '游永瓏', 'Benson-Yu', 151, 79, '2155', 'BensonYu@transglobe.com.tw', 34.6476, '2020-03-20 15:03:06', NULL),
	(33, '蘇輝慧', 'Chiahui-Su', 176, 66, '6177', 'ChiahuiSu@transglobe.com.tw', 21.3068, '2020-03-20 15:03:06', NULL),
	(34, '盧佳莉', 'Chili-Lu', 169, 66, '6177', 'chililu@transglobe.com.tw', 23.1084, '2020-03-20 15:03:06', NULL),
	(35, '徐琪銘', 'Colin-Hsu', 178, 58, '6121', 'ColinHsu@transglobe.com.tw', 18.3058, '2020-03-20 15:03:06', NULL),
	(36, '王志皓', 'Curious-Wang', 160, 58, '6174', 'CuriousWang@transglobe.com.tw', 22.6562, '2020-03-20 15:03:06', NULL),
	(37, '林祺娟', 'Dania-Lin', 186, 57, '6162', 'danialin@transglobe.com.tw', 16.4759, '2020-03-20 15:03:06', NULL),
	(38, '周秀華', 'Daniel-Chou', 161, 67, '6153', 'danielchou@transglobe.com.tw', 25.8478, '2020-03-20 15:03:06', NULL),
	(39, '陳正祺', 'David-Chen', 171, 89, '6109', 'davidchen@transglobe.com.tw', 30.4367, '2020-03-20 15:03:06', NULL),
	(40, '覃戴誠', 'David-Chin', 188, 66, '6187', 'DavidChin@transglobe.com.tw', 18.6736, '2020-03-20 15:03:06', NULL),
	(41, '孔智維', 'David-Kung', 152, 69, '6141', 'DavidKung@transglobe.com.tw', 29.865, '2020-03-20 15:03:06', NULL),
	(42, '王大迪', 'Dean-Wang', 183, 53, '6173', 'DeanWang@transglobe.com.tw', 15.8261, '2020-03-20 15:03:06', NULL),
	(43, '潘俊慧', 'Debby-Pan', 168, 50, '6137', 'debbypan@transglobe.com.tw', 17.7154, '2020-03-20 15:03:06', NULL),
	(44, '辛麗達', 'Eddy-Shin', 189, 73, '6184', 'EddyShin@transglobe.com.tw', 20.4362, '2020-03-20 15:03:06', NULL),
	(45, '吳易珍', 'Ella-Wu', 160, 53, '6124', 'ellawu@transglobe.com.tw', 20.7031, '2020-03-20 15:03:06', NULL),
	(46, '程佩恆', 'Eric-Cheng', 176, 61, '6152', 'ericcheng@transglobe.com.tw', 19.6927, '2020-03-20 15:03:06', NULL),
	(47, '林福安', 'Eric-Lin', 171, 57, '6163', 'ericlinja@transglobe.com.tw', 19.4932, '2020-03-20 15:03:06', NULL),
	(48, '林俊宏', 'Frank-Lin', 170, 55, '6168', 'FrankLin@transglobe.com.tw', 19.0311, '2020-03-20 15:03:06', NULL),
	(49, '陳宗豪', 'Freddy-Chen', 162, 76, '6191', 'FreddyChen@transglobe.com.tw', 28.959, '2020-03-20 15:03:06', NULL),
	(50, '張文維', 'Henry-Chang', 189, 83, '6135', 'HenryChang@transglobe.com.tw', 23.2356, '2020-03-20 15:03:06', NULL),
	(51, '潘宏貞', 'Hope-Pan', 166, 53, '6134', 'hopepan@transglobe.com.tw', 19.2336, '2020-03-20 15:03:06', NULL),
	(52, '許惠輝', 'Jack-Hsu', 162, 70, '6160', 'jackhsu@transglobe.com.tw', 26.6728, '2020-03-20 15:03:06', NULL),
	(53, '涂良敏', 'Jack-Tu', 164, 71, '6119', 'jacktu@transglobe.com.tw', 26.398, '2020-03-20 15:03:06', NULL),
	(54, '吳鏡民', 'James-Wu', 185, 73, '6107', 'jameswu@transglobe.com.tw', 21.3294, '2020-03-20 15:03:06', NULL),
	(55, '徐健杰', 'Jay-Hsu', 171, 58, '6183', 'JayHsu@transglobe.com.tw', 19.8352, '2020-03-20 15:03:06', NULL),
	(56, '羅民', 'Jay-Lo', 156, 62, '6997', 'jaylo@transglobe.com.tw', 25.4767, '2020-03-20 15:03:06', NULL);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
