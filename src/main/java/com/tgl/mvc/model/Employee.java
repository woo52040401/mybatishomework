package com.tgl.mvc.model;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Employee {

	private long id;

	@DecimalMax(value = "300.0", message = "身高必須小於等於300")
	@DecimalMin(value = "100.0", message = "身高必須大於等於100")
	private double tall;

	@DecimalMax(value = "200.0", message = "體重必須小於等於200")
	@DecimalMin(value = "20.0", message = "體重必須大於等於20")
	private double weight;

	@Size(min = 4, max = 30, message = "請輸入正確英文名字")
	private String enName;

	@Size(min = 2, max = 4, message = "請輸入正確中文名字")
	private String Name;

	@NotEmpty(message = "請輸入電話號碼")
	@Pattern(regexp = "^[0-9]{4}", message = "請輸入四位數號碼")
	private String phone;

	@Email(message = "Email 無效")
	@Size(max = 60, message = "Email長度不能超過60")
	private String email;

	private double bmi;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getTall() {
		return tall;
	}

	public void setTall(double tall) {
		this.tall = tall;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}
}
