package com.tgl.mvc.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.EmployeeService;

@Controller
@ResponseBody
@RequestMapping(value = "/rest/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@PostMapping
	public ResponseEntity<Long> insert(@RequestBody @Valid Employee employee){
		long createId = employeeService.insert(employee);
		return new ResponseEntity<>(employee.getId(), HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Long> delete(@PathVariable("id") @Min(1) long employeeId){
		Employee result = employeeService.findById(employeeId);
		if(result == null) {
			System.out.println("Record not found, id: " + employeeId);
		}
		employeeService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee){
		Employee result = employeeService.findById(employee.getId());
		if(result == null) {
			System.out.println("Record not found, id: " + employee.getId());
		}
		employeeService.update(employee);
	    return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Employee> findById(@PathVariable("id") @Min(1) long employeeId){
		Employee result = employeeService.findById(employeeId);
		if(result == null) {
			System.out.println("Record not found, id: " + employeeId);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
