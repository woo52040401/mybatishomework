package com.tgl.mvc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.tgl.mvc.model.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {
  public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
    Employee emp = new Employee();
    emp.setId(rs.getLong("id"));
    emp.setName(rs.getString("name"));
    emp.setEnName(rs.getString("enName"));
    emp.setTall(rs.getDouble("tall"));
    emp.setWeight(rs.getDouble("weight"));
    emp.setBmi(rs.getDouble("bmi"));
    emp.setEmail(rs.getString("email"));
    emp.setPhone(rs.getString("phone"));
    return emp;
  }
}
