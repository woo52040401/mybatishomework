package com.tgl.mvc.dao;


import org.springframework.stereotype.Repository;

import com.tgl.mvc.model.Employee;


@Repository
public interface EmployeeDao {
	
	Employee findById(long id);

	long insert(Employee employee);

	int update(Employee employee);

	boolean delete(long id);
}
